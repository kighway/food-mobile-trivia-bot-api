Rails.application.routes.draw do
  get 'webhook' => 'application#webhook', as:'webhook'
  mount Facebook::Messenger::Server, at: 'bot'
end
