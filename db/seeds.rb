# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#q = Question.create(type: "MultipleChoice", text: "What is your name?")
#q.options << Option.new(text: "Kyle", answer: true) #Answer
#q.options << Option.new(text: "Aidan")
#q.options << Option.new(text: "Toby")
#q.options << Option.new(text: "Hope")
#
#q = Question.create(type: "TrueFalse", text: "It is the year 2017.")
#q.options << Option.new(answer: true) #Answer

# ####### Trivia Questions #######

q = Question.create(type: "MultipleChoice", text: "When was Food Network founded?")
q.options << Option.new(text: "October 13, 2000")
q.options << Option.new(text: "November 22, 1993", answer: true) #Answer
q.options << Option.new(text: "April 10, 1995")
q.options << Option.new(text: "January 30, 1987")


q = Question.create(type: "MultipleChoice", text: "Where are the Food Network headquarters?")
q.options << Option.new(text: "Knoxville, TN")
q.options << Option.new(text: "New York City", answer: true) #Answer
q.options << Option.new(text: "Los Angeles")
q.options << Option.new(text: "Boston, MA")


q = Question.create(type: "MultipleChoice", text: "Which chef used to work in the White House?")
q.options << Option.new(text: "Bobby Flay")
q.options << Option.new(text: "Mario Batali")
q.options << Option.new(text: "Ina Garten", answer: true) #Answer
q.options << Option.new(text: "Giada De Laurentiis")


q = Question.create(type: "MultipleChoice", text: "Which company owns the majority of Food Network?")
q.options << Option.new(text: "Unilever")
q.options << Option.new(text: "Scripps Networks", answer: true) #Answer
q.options << Option.new(text: "Mondelez International")
q.options << Option.new(text: "Procter and Gamble")


q = Question.create(type: "MultipleChoice", text: "When did Bobby Flay join Food Network?")
q.options << Option.new(text: "1990")
q.options << Option.new(text: "1998")
q.options << Option.new(text: "2002")
q.options << Option.new(text: "1995", answer: true) #Answer


q = Question.create(type: "MultipleChoice", text: "Where does the Pioneer Woman live?")
q.options << Option.new(text: "Iowa")
q.options << Option.new(text: "Oklahoma", answer: true) #Answer
q.options << Option.new(text: "Kansas")
q.options << Option.new(text: "Georgia")


q = Question.create(type: "MultipleChoice", text: "What are the Food Network signature colors?")
q.options << Option.new(text: "Blue and Yellow")
q.options << Option.new(text: "Red and Green")
q.options << Option.new(text: "Red and White", answer: true) #Answer
q.options << Option.new(text: "Orange and Yellow")


q = Question.create(type: "MultipleChoice", text: "What is the Food Network slogan?")
q.options << Option.new(text: "Let’s Cook!")
q.options << Option.new(text: "Clear eyes, full stomachs, can’t lose")
q.options << Option.new(text: "Dig In!", answer: true) #Answer
q.options << Option.new(text: "Mangia!")


q = Question.create(type: "MultipleChoice", text: "Which of the following was not a real Food Network show?")
q.options << Option.new(text: "Emeril Live")
q.options << Option.new(text: "$40 A Day")
q.options << Option.new(text: "Good Eats")
q.options << Option.new(text: "Vegetarians United", answer: true) #Answer


q = Question.create(type: "MultipleChoice", text: "Which of the following did not star on Food Network when it first launched?")
q.options << Option.new(text: "David Rosengarten")
q.options << Option.new(text: "Donna Hanover")
q.options << Option.new(text: "Robin Leach")
q.options << Option.new(text: "Julia Child", answer: true) #Answer


q = Question.create(type: "MultipleChoice", text: "Who hosts Chopped?")
q.options << Option.new(text: "Ryan Seacrest")
q.options << Option.new(text: "Ted Allen", answer: true) #Answer
q.options << Option.new(text: "Amanda Freitag")
q.options << Option.new(text: "Ty Pennington")


q = Question.create(type: "MultipleChoice", text: "When is Ted Allen’s birthday?")
q.options << Option.new(text: "January 22")
q.options << Option.new(text: "October 1")
q.options << Option.new(text: "May 20", answer: true) #Answer
q.options << Option.new(text: "March 14")


q = Question.create(type: "MultipleChoice", text: "Who is married to Ladd?")
q.options << Option.new(text: "Ina Garten")
q.options << Option.new(text: "Ree Drummond", answer: true) #Answer
q.options << Option.new(text: "Katie Lee")
q.options << Option.new(text: "Giada De Laurentiis")


q = Question.create(type: "MultipleChoice", text: "What color is Ina Garten’s stand mixer?")
q.options << Option.new(text: "Yellow")
q.options << Option.new(text: "Red")
q.options << Option.new(text: "Blue")
q.options << Option.new(text: "White", answer: true) #Answer


q = Question.create(type: "MultipleChoice", text: "Which is Ina Garten’s favorite cocktail?")
q.options << Option.new(text: "Gin & Tonic")
q.options << Option.new(text: "Whiskey Sour", answer: true) #Answer
q.options << Option.new(text: "Moscow Mule")
q.options << Option.new(text: "Margarita")


q = Question.create(type: "MultipleChoice", text: "Which of the following did NOT make a guest appearance on the Barefoot Contessa show?")
q.options << Option.new(text: "Taylor Swift")
q.options << Option.new(text: "Elmo")
q.options << Option.new(text: "Neil Patrick Harris")
q.options << Option.new(text: "Ryan Gosling", answer: true) #Answer


q = Question.create(type: "MultipleChoice", text: "Where do Ina and Jeffrey live?")
q.options << Option.new(text: "Nantucket, MA")
q.options << Option.new(text: "New York, NY")
q.options << Option.new(text: "East Hampton, NY", answer: true) #Answer
q.options << Option.new(text: "Palm Beach, FL")


q = Question.create(type: "MultipleChoice", text: "How did Ina Garten choose the name Barefoot Contessa?")
q.options << Option.new(text: "It was her childhood nickname")
q.options << Option.new(text: "She once bought a specialty-food store with that name", answer: true) #Answer
q.options << Option.new(text: "She found it written in beach sand")
q.options << Option.new(text: "She dreamed that she met an Italian countess")


q = Question.create(type: "MultipleChoice", text: "This chef often says ‘How easy is that?’:")
q.options << Option.new(text: "Emeril Lagasse")
q.options << Option.new(text: "Rachael Ray")
q.options << Option.new(text: "Ina Garten", answer: true) #Answer
q.options << Option.new(text: "Trisha Yearwood")


q = Question.create(type: "TrueFalse", text: "Ina Garten initially said no to hosting her own Food Network cooking show.")
q.options << Option.new(answer: true) #Answer

q = Question.create(type: "MultipleChoice", text: "Which Chopped equipment has notoriously given chefs the most trouble?")
q.options << Option.new(text: "Whipped cream canister")
q.options << Option.new(text: "Deep-fryer")
q.options << Option.new(text: "Ice cream machine", answer: true) #Answer
q.options << Option.new(text: "Anti-griddle")


q = Question.create(type: "MultipleChoice", text: "What type of knife is featured in the Chopped logo?")
q.options << Option.new(text: "Cleaver", answer: true) #Answer
q.options << Option.new(text: "Carving knife")
q.options << Option.new(text: "Slicer")
q.options << Option.new(text: "Santoku knife")


q = Question.create(type: "MultipleChoice", text: "Each Iron Chef’s jacket is outfitted with ___ on the sleeve.")
q.options << Option.new(text: "His or her birth year")
q.options << Option.new(text: "The flag of his or her country of origin", answer: true) #Answer
q.options << Option.new(text: "His or her initials")
q.options << Option.new(text: "The mascot of his or her alma mater")


q = Question.create(type: "MultipleChoice", text: "Which criterion is most important on Iron Chef America?")
q.options << Option.new(text: "Presentation")
q.options << Option.new(text: "Taste", answer: true) #Answer
q.options << Option.new(text: "Originality")
q.options << Option.new(text: "Time to Finish")


q = Question.create(type: "MultipleChoice", text: "Which Iron Chef was the first and only Iron Chef to do battle alone, without sous chefs?")
q.options << Option.new(text: "Michael Symon", answer: true) #Answer
q.options << Option.new(text: "Bobby Flay")
q.options << Option.new(text: "Masaharu Morimoto")
q.options << Option.new(text: "Cat Cora")


q = Question.create(type: "MultipleChoice", text: "Which Iron Chef was the first in the series’ history to earn a perfect score in battle?")
q.options << Option.new(text: "Iron Chef Garces", answer: true) #Answer
q.options << Option.new(text: "Iron Chef Morimoto")
q.options << Option.new(text: "Iron Chef Batali")
q.options << Option.new(text: "Iron Chef Forgione")


q = Question.create(type: "MultipleChoice", text: "Which of the following is NOT a co-host on The Kitchen?")
q.options << Option.new(text: "Katie Lee")
q.options << Option.new(text: "Marcela Valladolid")
q.options << Option.new(text: "Sunny Anderson")
q.options << Option.new(text: "Alex Guarnaschelli", answer: true) #Answer


q = Question.create(type: "MultipleChoice", text: "What is the color of the cabinets in The Kitchen?")
q.options << Option.new(text: "Green")
q.options << Option.new(text: "Blue", answer: true) #Answer
q.options << Option.new(text: "White")
q.options << Option.new(text: "Pink")


q = Question.create(type: "MultipleChoice", text: "Which country singer is also a Food Network chef?")
q.options << Option.new(text: "Carrie Underwood")
q.options << Option.new(text: "Trisha Yearwood", answer: true) #Answer
q.options << Option.new(text: "Miranda Lambert")
q.options << Option.new(text: "Dolly Parton")


q = Question.create(type: "MultipleChoice", text: "Finish the title: Bakers vs. ___")
q.options << Option.new(text: "Shakers")
q.options << Option.new(text: "Takers")
q.options << Option.new(text: "Fakers", answer: true) #Answer
q.options << Option.new(text: "Cakers")


q = Question.create(type: "MultipleChoice", text: "Finish the title: ___ @ Bobby’s")
q.options << Option.new(text: "Breakfast")
q.options << Option.new(text: "Brunch", answer: true) #Answer
q.options << Option.new(text: "Lunch")
q.options << Option.new(text: "Dinner")


q = Question.create(type: "MultipleChoice", text: "Which sweet show is the oldest?")
q.options << Option.new(text: "Ace of Cakes", answer: true) #Answer
q.options << Option.new(text: "Cake Wars")
q.options << Option.new(text: "Cupcake Wars")
q.options << Option.new(text: "Dessert First")


q = Question.create(type: "MultipleChoice", text: "On which show did Alton Brown appear first?")
q.options << Option.new(text: "Good Eats", answer: true) #Answer
q.options << Option.new(text: "Cutthroat Kitchen")
q.options << Option.new(text: "Iron Chef America")
q.options << Option.new(text: "Feasting on Asphalt")


q = Question.create(type: "MultipleChoice", text: "Which of the following is a judge on Cutthroat Kitchen?")
q.options << Option.new(text: "Antonia Lofaso", answer: true) #Answer
q.options << Option.new(text: "Bobby Flay")
q.options << Option.new(text: "Valerie Bertinelli")
q.options << Option.new(text: "Guy Fieri")


q = Question.create(type: "MultipleChoice", text: "Which of the following shows does not feature Guy Fieri?")
q.options << Option.new(text: "Diners, Drive-Ins and Dives")
q.options << Option.new(text: "Guy’s Grocery Games")
q.options << Option.new(text: "Cake Boss", answer: true) #Answer
q.options << Option.new(text: "Guy’s Big Bite")


q = Question.create(type: "MultipleChoice", text: "Which of the following are Giada De Laurentiis shows?")
q.options << Option.new(text: "Everyday Italian")
q.options << Option.new(text: "Giada at Home")
q.options << Option.new(text: "Farmhouse Rules")
q.options << Option.new(text: "Both A and B", answer: true) #Answer


q = Question.create(type: "MultipleChoice", text: "Food Network is available on which of the following platforms?")
q.options << Option.new(text: "Facebook")
q.options << Option.new(text: "Snapchat")
q.options << Option.new(text: "Apple TV")
q.options << Option.new(text: "All of the above", answer: true) #Answer


q = Question.create(type: "MultipleChoice", text: "What was Sunny Anderson’s occupation before coming to Food Network?")
q.options << Option.new(text: "Radio personality", answer: true) #Answer
q.options << Option.new(text: "School teacher")
q.options << Option.new(text: "Playwright")
q.options << Option.new(text: "Actress")
