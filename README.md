# README

i'm the api that a facebook bot uses to allow a user to play food network trivia game
developer account:
https://developers.facebook.com/apps/1853319241650546
public facing facebook page:
https://www.facebook.com/choppedbot

tim has admin rights and passwords to these accounts plus the heroku account where you can develop me

you also need secret, api, auth, and verify keys. the app looks for them in the local_env file

both marcus and tim have these files as well as admin access to the bitbucket accounts

the local_env file must be called local_env.yml and in goes in /config
"afternoon-coast-95912" is the name of the original heroku app this was hosted on

If your build fails to communicate with Facebook successfully, FB auto-unsubscribes the bot--  to resubscribe, in:
heroku run rails console
Run the following:
Facebook::Messenger::Subscriptions.subscribe(access_token: ENV['ACCESS_TOKEN'])
reconnection sometimes takes a minute or two, so be patient in between tests.

drawing random Questions depends on SQL row id to prevent repetition
therefore any new migrations/seeds need to be run INDIVIDUALLY, or else in downtime do this before a full migration and reseeding:
heroku pg:reset
<add your new seed file to new git commit and push it to heroku>
heroku run rake db:migrate
heroku run rake db:seed

ERROR: "Heroku: Cannot run more than 1 Free size dynos"
heroku ps
heroku ps:stop (<running_dino_name>) #run.9382

"testing in production:"
to test a local branch "testbranch" temporarily on heroku
instead of git push heroku master
for the first time switching heroku from master to your your "testbranch"
git push -f heroku testbranch:master
then when merges can be automatic:
git push heroku testbranch:master
to reinstate your working master branch to heroku
git push -f heroku master
then when merges can be automatic:
git push heroku master

#<URI::InvalidURIError: URI must be ascii only>
Question strings must only be composed of ASCII chars

GET STARTED BUTTON/ PERSISTENT MENU:
see readme--get-started-persistent-menu.rb for the current commands and to update them
