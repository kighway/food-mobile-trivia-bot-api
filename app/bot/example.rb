# app/bot/example.rb

include Facebook::Messenger

Bot.on :message do |message|
  Reply.new(postback: message).play?
end

Bot.on :postback do |postback|
  payload = JSON.parse(postback.payload)
  case payload["status"]
  when "onBoard"
    Reply.new(postback: postback, payload: payload).ask_question
  when "inProgress"
    reply = Reply.new(postback: postback, payload: payload)
    if payload["attempts"] == 14
      reply.end_game
      reply.replay?
    elsif payload["choice"] == payload["answer"]
      reply.correct
      reply.ask_question
    else
      reply.incorrect
      reply.ask_question
    end
  end
end
