class Payload

  attr_reader :status, :payload

  def initialize(status: , question_id: nil, query: nil, choice: nil, answer: nil, score: nil, attempts: nil)
    @status = status
    @query = query
    @question_id = question_id
    @choice = choice
    @answer = answer
    @score = score
    @attempts = attempts
  end

  def payload
      {
        "status" => @status,
        "query" => @query,
        "questionID" => @question_id,
        "choice" => @choice,
        "answer" => @answer,
        "score" => @score,
        "attempts" => @attempts
      }.to_json
  end
end
