class Reply

  attr_reader :postback, :payload

  def initialize (postback: , payload: nil)
    @payload = payload
    @postback = postback
  end

  def postback_button(text: , accept_status: , accept_text: , score: nil, attempts: nil )
      {
        attachment: {
          type: 'template',
          payload: {
            template_type: 'button',
            text: text,
            buttons:
            [
              {
                type: 'postback',
                title: accept_text,
                payload: Payload.new(
                  status: accept_status,
                  score: score,
                  attempts: attempts
                ).payload
              }
            ]

          }
        }
      }
  end

  def play?
    postback.reply(text: "Welcome to the Unofficial Food Network Trivia Quiz!")
    postback.reply(text: "We will ask you a total of 15 questions.")
    postback.reply(
      postback_button(
        text: 'Want to test your knowledge?',
        accept_status: "onBoard",
        accept_text: "Yes!",
        score: 0,
        attempts: 0
      )
    )
  end

  def ask_question
    question = Question.random
    postback.reply(text: question.text)
    option_elements = question.options_text.map.with_index do |text, index|
      {
        "title" => text,
        buttons:
        [
          {
            title: "Select.",
            type: 'postback',
            payload: Payload.new(
              status: "inProgress",
              question_id: question.id,
              choice: index,
              answer: question.answer_index,
              score: payload["score"],
              attempts: payload["attempts"]
            ).payload
          }
        ]
      }
    end
    postback.reply(
        {
          attachment: {
            type: 'template',
            payload: {
              template_type: 'list',
              top_element_style: 'compact',
              elements: option_elements
            }
          }
        }
      )
  end

  def correct
    payload["attempts"] += 1
    payload["score"] += 1
    postback.reply(text: "Nice! You got it right!")
    postback.reply(text: "Your score is now #{payload["score"]}.")
    postback.reply(text: "So far, you've attempted #{payload["attempts"]} question#{payload["attempts"] > 1 ? 's' : ''} out of the total 15.")
  end

  def incorrect
    payload["attempts"] += 1
    answer_text = Question.find(payload["questionID"]).answer_text
    postback.reply(text: "Ah, too bad, the correct answer was actually: '#{ answer_text }'.")
    postback.reply(text: "You score is still #{payload["score"]}.")
    postback.reply(text: "So far, you've attempted #{payload["attempts"]} questions out of the total 15.")
  end

  def end_game
    postback.reply(text: "You answered #{payload["score"]} out of 15 questions correctly...")
    reply_text = case payload["score"]
    when 15
      "You are literally perfect!"
    when 12..14
      "Great job!"
    when 9..11
      "Pretty good!"
    when 5..8
      "Plenty of room for improvement!"
    when 1..4
      "You must be new to Food Network. Welcome!"
    when 0
      "You're supposed to pick the RIGHT answers..."
    end
    postback.reply(text: reply_text)
  end

  def replay?
    postback.reply(
      postback_button(
        text: 'Want to play again?',
        accept_status: "onBoard",
        accept_text: "Yes!",
        score: 0,
        attempts: 0
      )
    )
  end
end
