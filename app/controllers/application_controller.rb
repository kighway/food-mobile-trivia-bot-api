class ApplicationController < ActionController::API
  def webhook
    if request.params['hub.mode'] == 'subscribe' && request.params['hub.verify_token'] == ENV['VERIFY_TOKEN']
      render json: request.params['hub.challenge']
      return
    else
      render status: 403, json: { "message": "bad request params" }
      return
    end
  end
end
