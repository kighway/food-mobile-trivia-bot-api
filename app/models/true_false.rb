class TrueFalse < Question
  def answer
      options.first.answer
  end

  def guess(guess_truth_value)
    if guess_truth_value == answer
      "Great Job!"
    else
      "Sorry, the statement was #{answer}."
    end
  end
end
