class MultipleChoice < Question
  def options_text
    options.map{ |option| option.text }
  end

  def answer
    options.find{ |option| option.answer }
  end

  def answer_text
    answer.text
  end

  def answer_index
    options.index{ |option| option.answer }
  end
end
