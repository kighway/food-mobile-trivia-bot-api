class Question < ApplicationRecord
  has_many :options

  def self.random
    if self == Question
      Question.order("RANDOM()").limit(1)[-1]
    else
      Question.where("type = ?", self).order("RANDOM()").limit(1)[-1]
    end
  end
end
