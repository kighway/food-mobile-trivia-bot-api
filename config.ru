# This file is used by Rack-based servers to start the application.

require_relative 'config/environment'
require 'facebook/messenger'
require_relative 'app/bot/example'
require_relative 'app/bot/reply'
require_relative 'app/bot/payload'


run Rails.application
run Facebook::Messenger::Server
